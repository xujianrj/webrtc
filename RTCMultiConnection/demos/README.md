## [RTCMultiConnection.js](http://www.rtcmulticonnection.org/) / WebRTC Library

## [Demos](https://www.webrtc-experiment.com/RTCMultiConnection/) using [RTCMultiConnection](http://www.RTCMultiConnection.org/)

<ol>
                <li><a href="https://www.webrtc-experiment.com/RTCMultiConnection/AppRTC-Look.html">AppRTC like RTCMultiConnection demo!</a></li>
                <li><a href="https://www.webrtc-experiment.com/RTCMultiConnection/MultiRTC/">MultiRTC! RTCMultiConnection all-in-one demo!</a></li>
                <li><a href="https://www.webrtc-experiment.com/Canvas-Designer/">Collaborative Canvas Designer</a></li>
				<li><a href="https://www.webrtc-experiment.com/RTCMultiConnection/all-in-one.html">All-in-One test</a></li>
                <li><a href="https://www.webrtc-experiment.com/RTCMultiConnection/Multi-Broadcasters-and-Many-Viewers.html">Multi-Broadcasters and Many Viewers</a></li>
                <li><a href="https://www.webrtc-experiment.com/RTCMultiConnection/select-broadcaster-at-runtime.html">Select Broadcaster at runtime</a></li>
                <li><a href="https://www.webrtc-experiment.com/RTCMultiConnection/OneWay-Screen-TwoWay-Audio.html">OneWay Screen & Two-Way Audio</a></li>
                <li><a href="https://www.webrtc-experiment.com/RTCMultiConnection/stream-mp3-live.html">Stream Mp3 Live</a></li>
                <li><a href="https://www.webrtc-experiment.com/RTCMultiConnection/socketio-auto-open-join-room.html">Socket.io auto Open/Join rooms</a></li>
                <li><a href="https://www.webrtc-experiment.com/RTCMultiConnection/getMediaDevices.html">navigator.getMediaDevices / navigator.enumerateDevices / MediaStreamTrack.getSources</a></li>
				<li><a href="https://www.webrtc-experiment.com/RTCMultiConnection/Renegotiation.html">Renegotiation & Mute/UnMute/Stop</a></li>
				<li><a href="https://www.webrtc-experiment.com/RTCMultiConnection/videoconferencing.html">Video-Conferencing</a></li>
				<li><a href="https://www.webrtc-experiment.com/RTCMultiConnection/video-broadcasting.html">Video Broadcasting</a></li>
                <li><a href="https://www.webrtc-experiment.com/RTCMultiConnection/audioconferencing.html">Audio Conferencing</a></li>
                <li><a href="https://www.webrtc-experiment.com/RTCMultiConnection/multi-streams-attachment.html">Multi-streams attachment</a></li>
				<li><a href="https://www.webrtc-experiment.com/RTCMultiConnection/admin-guest.html">Admin/Guest audio/video calling</a></li>
				<li><a href="https://www.webrtc-experiment.com/RTCMultiConnection/session-reinitiation.html">Session Re-initiation Test</a></li>
				<li><a href="https://www.webrtc-experiment.com/RTCMultiConnection/rooms-screenshots.html">Preview Screenshot of the room</a></li>
				<li><a href="https://www.webrtc-experiment.com/RTCMultiConnection/RecordRTC-and-RTCMultiConnection.html">RecordRTC & RTCMultiConnection</a></li>
                <li><a href="https://www.webrtc-experiment.com/RTCMultiConnection/features.html">Explains how to customize ice servers; and resolutions</a></li>
                <li><a href="https://www.webrtc-experiment.com/RTCMultiConnection/mute-unmute.html">Mute/Unmute and onmute/onunmute</a></li>
                <li><a href="https://www.webrtc-experiment.com/RTCMultiConnection/one-page-demo.html">One-page demo: Explains how to skip external signalling gateways</a></li>
                <li><a href="https://www.webrtc-experiment.com/RTCMultiConnection/password-protect-rooms.html">Password Protect Rooms: Explains how to authenticate users</a></li>
                <li><a href="https://www.webrtc-experiment.com/RTCMultiConnection/session-management.html">Session Management: Explains difference between "leave" and "close" methods</a></li>
                <li><a href="https://www.webrtc-experiment.com/RTCMultiConnection/multi-sessions-management.html">Multi-Sessions Management</a></li>
				<li><a href="https://www.webrtc-experiment.com/RTCMultiConnection/RTCMultiConnection-v1.3-demo.html">RTCMultiConnection-v1.3 test</a></li>
				<li><a href="https://www.webrtc-experiment.com/RTCMultiConnection/bandwidth.html">Customizing Bandwidth</a></li>
				<li><a href="https://www.webrtc-experiment.com/RTCMultiConnection/users-ejection.html">Users ejection and presence detection</a></li>				
                <li><a href="https://www.webrtc-experiment.com/RTCMultiConnection/multi-session-establishment.html">Multi-Session Establishment</a></li>
                <li><a href="https://www.webrtc-experiment.com/RTCMultiConnection/group-file-sharing-plus-text-chat.html">File Sharing + Text Chat</a></li>
                <li><a href="https://www.webrtc-experiment.com/RTCMultiConnection/audio-conferencing-data-sharing.html">Audio Conferencing + File Sharing + Text Chat</a></li>
                <li><a href="https://www.webrtc-experiment.com/RTCMultiConnection/join-with-or-without-camera.html">Join with/without camera</a></li>
                <li><a href="https://www.webrtc-experiment.com/RTCMultiConnection/screen-sharing.html">Screen Sharing</a></li>
                <li><a href="https://www.webrtc-experiment.com/RTCMultiConnection/one-to-one-filesharing.html">One-to-One file sharing</a></li>
                <li><a href="https://www.webrtc-experiment.com/RTCMultiConnection/manual-session-establishment-plus-extra-data-transmission.html">Manual session establishment + extra data transmission</a></li>
                <li><a href="https://www.webrtc-experiment.com/RTCMultiConnection/manual-session-establishment-plus-extra-data-transmission-plus-videoconferencing.html">Manual session establishment + extra data transmission + video conferencing</a></li>
</ol>

## License

[RTCMultiConnection](https://github.com/muaz-khan/RTCMultiConnection) is released under [MIT licence](https://www.webrtc-experiment.com/licence/) . Copyright (c) [Muaz Khan](https://plus.google.com/+MuazKhan).
